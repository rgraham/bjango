"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.

Good Times!
"""

from django.test import TestCase
import json, urllib
from bjango.models import Experiment, Alternative

class AlternativeAndExperimentTest(TestCase):
    def setUp(self):
        # These could be in a fixture and should be if it gets
        # more complex or we add tests that require these
        self.experiment = Experiment(name="Test", status="ongoing")
        self.experiment.save()
        self.alt1 = Alternative(content="alt1", experiment=self.experiment)
        self.alt2 = Alternative(content="alt2", experiment=self.experiment)

        self.alt1.visitors = 145
        self.alt1.views = 231
        self.alt1.conversions = 12

        self.alt2.visitors = 124
        self.alt2.views = 221
        self.alt2.conversions = 6

        self.alt1.save()
        self.alt2.save()
    
    def test_conversion_rate(self):
        self.assertAlmostEqual(self.alt1.conversion_rate(),
                self.alt1.conversions/float(self.alt1.visitors))
        self.assertAlmostEqual(self.alt2.conversion_rate(),
                self.alt2.conversions/float(self.alt2.visitors))

        # Check for division by zero
        self.alt2.visitors = 0
        self.assertEqual(0, self.alt2.conversion_rate())

    def test_p_value(self):
        self.assertEqual(self.experiment.p_value(),
                None)
        self.alt1.conversions = 22
        self.alt1.save()
        self.assertEqual(self.experiment.p_value(),
                0.01)

    def test_zscore(self):
        self.assertAlmostEqual(self.experiment.zscore(),
                1.14900854571)
        self.alt1.conversions = 22
        self.alt1.save()
        self.assertAlmostEqual(self.experiment.zscore(),
                2.91240480023)

        # Check no visitors
        self.alt1.visitors = 0
        self.alt2.visitors = 0
        self.alt1.save()
        self.alt2.save()
        self.assertRaises(RuntimeError, self.experiment.zscore)

        # Check != 2 alternatives
        self.alt2.delete()
        self.assertRaises(RuntimeError, self.experiment.zscore)

    def test_is_significant(self):
        self.assertEqual(self.experiment.is_significant(),
                False)
        self.alt1.conversions = 22
        self.alt1.save()
        self.assertEqual(self.experiment.is_significant(),
                True)


