from django.contrib.auth.models import SiteProfileNotAvailable
"""
Decorators that DRY up repeated functionality.
"""

def bjango_identity(f):
    """
    Decorator that replaces login_required for
    determining that a logged in user is also
    subscribed or under evaluation time.
    """
    def decorator(request, *args, **kwargs):
        try:
            if not request.user.is_authenticated():
                # the user is not logged in

                # Here we make sure the session exists
                # if it does not, we prompt it and then capture the key
                if request.session.session_key is None:
                    request.session.save()
                request.session['bjango_ident'] = request.session.session_key
        except SiteProfileNotAvailable:
            pass

        try:
            if request.user.is_authenticated() and not request.user.get_profile():
                # the user is logged in
                request.session['bjango_ident'] = request.user.username
        except SiteProfileNotAvailable:
            pass

        return f(request, *args, **kwargs)
            
    return decorator
