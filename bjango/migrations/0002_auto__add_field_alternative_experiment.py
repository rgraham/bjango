# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Alternative.experiment'
        db.add_column('bjango_alternative', 'experiment',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['bjango.Experiment']),
                      keep_default=False)

    def backwards(self, orm):
        # Deleting field 'Alternative.experiment'
        db.delete_column('bjango_alternative', 'experiment_id')

    models = {
        'bjango.alternative': {
            'Meta': {'object_name': 'Alternative'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'conversions': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'experiment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['bjango.Experiment']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'views': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'weight': ('django.db.models.fields.IntegerField', [], {})
        },
        'bjango.experiment': {
            'Meta': {'object_name': 'Experiment'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['bjango']