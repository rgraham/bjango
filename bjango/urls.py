from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'bjango.views.home', name='home'),
    url(r'^convert$', 'bjango.views.convert', name='convert'),
    url(r'^results$', 'bjango.views.results', name='results'),
    url(r'^register$', 'bjango.views.register', name='register'),
    url(r'^activate/(?P<activation_key>\w+)/$', 'bjango.views.activate', 
        name='activate'),
    
    # url(r'^bjango/', include('bjango.foo.urls')),
    (r'^accounts/', include('registration.backends.default.urls')),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', 
        {'template_name': 'registration/login.html'}),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
