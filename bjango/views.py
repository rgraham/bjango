from django.template import RequestContext
from django.shortcuts import render_to_response
from models import Experiment, Alternative
from django.template import TemplateDoesNotExist
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required

from decorators import bjango_identity
from utils import split_test, conversion
from registration.views import register as _register
from registration.views import activate as _activate

@bjango_identity
def home(request):
    text = split_test(['bjango one', 'bjango two'], 'meaningless', 
            request.session.get('bjango_ident', ''))
    return render_to_response("home.html", {'request':request, 
        'text': text}, context_instance=RequestContext(request))

@bjango_identity
def convert(request):
    conversion('meaningless', request.session.get('bjango_ident', ''))
    return render_to_response("convert_from_home.html", {'request':request},
            context_instance=RequestContext(request))

@login_required(login_url='/accounts/login/')
def results(request):
    if request.user.is_superuser:
        experiments = Experiment.objects.all()
        return render_to_response("results.html", {'experiments':experiments},
                context_instance=RequestContext(request))
    else:
        return render_to_response("denied.html", {},
                context_instance=RequestContext(request))


 # register(request, backend, success_url=None, form_class=None,
 #            disallowed_url='registration_disallowed',
 #            template_name='registration/registration_form.html',
 #            extra_context=None)
@bjango_identity
def register(request):
    reg_template = split_test(['registration/registration_form.html', 
        'register.html'], 
        'registration',
        request.session.get('bjango_ident', ''))

    return _register(request, 'registration.backends.default.DefaultBackend',
            template_name=reg_template)

@bjango_identity
def activate(request, activation_key):
    conversion('registration', request.session.get('bjango_ident', ''))

    return _activate(request, 'registration.backends.default.DefaultBackend',
            activation_key=activation_key)
