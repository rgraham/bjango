import datetime
from random import choice
import logging

from models import Experiment, Alternative, Participant

logger = logging.getLogger(__name__)

def split_test(alternatives, name, ident, automatic_winner=False):
    """
    This instantiates a test, creates the models, selects an alternative,
    stores the alternative, returns the alternative, and sets a cookie or other
    identification method
    """
    experiments = Experiment.objects.filter(name=name)
    if (experiments.count() == 0):
        # create the experiment and alternatives
        experiment = Experiment(name=name, status="Ongoing")
        experiment.save()

        for alt in alternatives:
            # TODO weights, dicts
            alternative = Alternative(content=alt, experiment=experiment) 
            alternative.save()
    else:
        experiment = experiments[0]
    
    # Store the alternative, if required
    participants = Participant.objects.filter(experiment=experiment, ident=ident)
    if participants.count() > 0:
        alt = participants[0].alternative
        selected_alt = alt.content
        unique = False
    else:
        # Select or discover the alternative
        selected_alt = choice(alternatives)
        alt = experiment.alternative_set.filter(content=selected_alt)[0]
        participant = Participant(experiment=experiment, ident=ident, 
                alternative=alt)
        participant.save()
        unique = True


    # Set a cookie or use logged in identity -- this is done in the decorator
    # Mark a view for the alternative
    alt.views += 1
    # Visitors measures the "unique" visitors to the page 
    if unique:
        alt.visitors += 1
    alt.save()

    # Return the alternative selected
    return selected_alt

def conversion(name, ident):
    """
    This function marks the named experiment/alternative combo as converted
    for the current id.

    It looks up the alternative and marks a conversion.
    """
    experiments = Experiment.objects.filter(name=name)
    if experiments.count() == 0:
        # This experiment does not yet exist. This should not happen.
        logger.warn("A conversion was called for before the split test began.")
        return

    experiment = experiments[0]
    participants = Participant.objects.filter(experiment=experiment, ident=ident)
    if participants.count() > 0:
        alt = participants[0].alternative
        alt.conversions += 1
        alt.save()
    else:
        logger.error("No participant exists for a conversion. Does the split test ecist?")


