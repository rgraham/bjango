"""
Based on:
    http://www.bingocardcreator.com/abingo/
    http://20bits.com/article/statistical-analysis-and-ab-testing

    TODOs
    - Report about where it is and why.
    - Fix zscore exceptions on results page
    
"""

Z_SCORE_MAPPING = [[0.10, 1.29], [0.05, 1.65], [0.01, 2.33], [0.001, 3.08]]

PERCENTAGES = {0.10 : "90%", 0.05 : "95%", 0.01 : "99%", 0.001 : "99.9%"}

CONFIDENCE_IN_WORDS = {0.10 : "somewhat confident", 0.05 : "confident",
        0.01 : "very confident", 0.001 : "extremely confident"}

from django.db import models
import datetime
import logging

logger = logging.getLogger(__name__)

class Experiment(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=255, unique=True, db_index=True)
    status = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name

    def zscore(self):
        if self.alternative_set.count() != 2:
           raise RuntimeError("Unfortunately, we can only manage to automatically calculate stats for 2 competing alternatives.")

        # TODO cleanup
        alt1 = self.alternative_set.all()[0]
        alt2 = self.alternative_set.all()[1]
        if alt1.visitors == 0 or alt2.visitors == 0:
            raise RuntimeError("The alternatives must have some visitors in order to calculate a z score.")

        conversion_rate1 = alt1.conversion_rate()
        conversion_rate2 = alt2.conversion_rate()

        visitors1 = alt1.visitors
        visitors2 = alt2.visitors

        numerator = conversion_rate1 - conversion_rate2
        denom1 = conversion_rate1 * (1 - conversion_rate1) / visitors1
        denom2 = conversion_rate2 * (1 - conversion_rate2) / visitors2

        if (denom1 + denom2) == 0 or (denom1 + denom2) < 0:
            return 0
        return numerator / ((denom1 + denom2) ** 0.5)

    def p_value(self):
        index = 0
        zscore = self.zscore()
        zscore = abs(zscore)
        found_p = None
        while index < len(Z_SCORE_MAPPING):
            if (zscore > Z_SCORE_MAPPING[index][1]):
               found_p = Z_SCORE_MAPPING[index][0]
            index += 1
        return found_p

    def is_significant(self, p = 0.05):
        if self.p_value() is None:
            return False
        return self.p_value() <= p

    def ready_for_testing(self):
        try:
            self.zscore()
        except:
            return False
        return True

class Alternative(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    weight = models.IntegerField(default=1)
    content = models.TextField(db_index=True)
    views = models.IntegerField(default=0)
    visitors = models.IntegerField(default=0)
    conversions = models.IntegerField(default=0)
    experiment = models.ForeignKey(Experiment)

    def __unicode__(self):
        return "Content size: %d Views: %d Experiment: %s" % (len(self.content), self.views, self.experiment)

    def conversion_rate(self):
        if self.visitors == 0:
            return 0.0
        return float(self.conversions)/float(self.visitors)
    
class Participant(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    alternative = models.ForeignKey(Alternative)
    ident = models.CharField(max_length=255, db_index=True)
    experiment = models.ForeignKey(Experiment)
    
